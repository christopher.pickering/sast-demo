# Gitlab CI Template - Add SAST and Secret Detection output as Merge Reqeust Comment

The free tier of Gitlab self hosted includes SAST testing and Secret Detection tools for your repos. However, the output is a pain in the neck to use.

This repo contains a Gitlab CI template that can be used to add the output as a comment to your merge reqeusts!

Here's how it looks!
![image.png](./image.png)

All we need to add to the project's ci is a include to the template!
```bash
include:
  - project: 'gitlab-instance-794cbcbc/templates'
    ref: master
    file: 'sast.yml'
```

And the comments will be auto added when ever you are pushing on a merge requets.

## How to Set it up?!

### Get a Access Token

I set this up with a bot user. You can use any user that has access to the groups/projects you wanna run the pipe on.

I also made the bot an admin so it can access any project.

![image-1.png](./image-1.png)

Go to the impersonation tab and create an Access Token with access to the API. Also, note the user's id. This will be needed in the runner setup.

The pipeline will delete any previous comment posted by this user (why its a good idea to use a dedicated bot) and replace them with the latest SAST results.

### Create a Runner

Next, create a new runner that will use the token.

In gitlab > admin > runners you can find the command to create a new runner. When prompted for a tag, put `merge-bot`.

After creating, edit the runner config file on your server. In my case ubuntu:

```bash
sudo nano /etc/gitlab-runner/config.toml
```
The runner code should look like this:

```bash
[[runners]]
  name = "merge-bot"
  url = "https://git.example.net/"
  token = "your token from gitlab>admin>runners"
  tls-ca-file = "/etc/gitlab/ssl/custom.git.example.net.crt"
  executor = "docker"
  environment = [
    "GIT_DEPTH=10",
    "API_TOKEN=<api token for merge-bot user",
    "GIT_SSL_NO_VERIFY=true",
    "AUTHOR_ID=<author id. ex:6>"
  ]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
### Create a Repo for the Templates

I used the main `Gitlab Instance` group and added a new project called `Templates`.

Copy the file `sast.yml` from this repo into there.

### Include it in your project

Finally, include 
```bash
include:
  - project: 'gitlab-instance-794cbcbc/templates'
    ref: master
    file: 'sast.yml'
```
in your projects ci template.

Try a merge request!
